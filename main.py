import os

from telegram import Update
from telegram.ext import Updater, MessageHandler, Filters, CallbackContext


def echo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    print(update.message)
    if update.message.from_user.id == 136817688 and update.message.from_user.is_bot:
        print("FOUND SOMEONE")
        w = updater.bot.delete_message(update.message.chat.id, update.message.message_id)
        print(w)
        w = updater.bot.ban_chat_sender_chat(update.message.chat.id, update.message.sender_chat.id)
        print(w)
        updater.bot.send_message(update.message.chat.id,
                                 "Пронырливый смертный решил использовать сообщения от своего канала, "
                                 "сообщение удалил")


def main() -> None:
    token = os.getenv("TOKEN")

    if not token or token == "1":
        print("I NEED TELEGRAM BOT TOKEN")
        exit()

    global updater
    updater = Updater(token)

    dispatcher = updater.dispatcher

    # on non command i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()